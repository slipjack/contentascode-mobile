import React from 'react';
import { StatusBar, Image, View, StyleSheet } from 'react-native';
import { Header, Left, Right, Body, Title, Button, Icon, Container, Content, Text, List, ListItem } from 'native-base';
import { compose, withState, withHandlers, pure } from 'recompose';
import { withNavigation } from 'react-navigation';

import Markdown, { getUniqueID } from 'react-native-markdown-renderer';

import markdown from '../styles/markdown';
import styles from './styles';
import index from '../../content';

const pages = withState('pages', 'setPages', ({ navigation: { state: { params: { pages } } } }) => pages);

const current = compose(
  withState('current', 'setCurrent', ({ navigation: { state: { params: { idx } } } }) => idx),
  withHandlers({
    next: ({ setCurrent, navigation: { state: { params: { pages } } } }) => () =>
      setCurrent(n => (n >= pages.length - 1 ? n : n + 1)),
    previous: ({ setCurrent }) => () => setCurrent(n => (n <= 0 ? n : n - 1))
  })
);

const rules = images => ({
  image: (node, children, parent, styles) => {
    if (!images[node.attributes.src]) return;
    // const width = Expo.Asset.fromModule(images[node.attributes.src]).width;
    // const heigh = Expo.Asset.fromModule(images[node.attributes.src]).height;
    return (
      // <View style={{ flex: 1, flexDirection: 'row' }} >
      <Image
        key={getUniqueID()}
        resizeMode="contain"
        style={StyleSheet.flatten([
          // {
          //   flex: 1
          // },
          styles.image
        ])}
        source={images[node.attributes.src]}
      />
      // </View>
    );
  }
});

const PreviousNext = ({ previous, next, pages, current }) => (
  <View
    style={{
      flexDirection: 'row',
      flex: 1,
      bottom: 0,
      left: 0,
      right: 0,
      justifyContent: 'space-between',
      padding: 15
    }}>
    {current > 0 ? (
      <Button small iconLeft light onPress={() => previous()}>
        <Icon name="arrow-back" />
        <Text>Previous</Text>
      </Button>
    ) : null}
    {current < pages.length - 1 ? (
      <Button small iconRight light onPress={() => next()}>
        <Text>Next</Text>
        <Icon name="arrow-forward" />
      </Button>
    ) : null}
  </View>
);

const LessonComponent = ({ current, previous, next, pages, navigation }) => {
  console.log('current', current);
  const { md, images } = index[pages[current].path];
  return (
    <Container style={{ marginTop: StatusBar.currentHeight }}>
      <Header style={styles.header}>
        <Left>
          <Button
            transparent
            onPress={() => {
              navigation.goBack();
            }}>
            <Icon
              name="arrow-back"
              style={{
                marginRight: 0,
                paddingRight: 5,
                paddingLeft: 0,
                marginLeft: 0,
                left: -2,
                bottom: -2,
                color: '#000'
              }}
            />
            <Text style={{ marginLeft: 0, paddingLeft: 0, left: -2, bottom: -3, color: '#000' }}>Top</Text>
          </Button>
        </Left>
        <Body>
          <Title style={styles.title}>{pages[current].title}</Title>
        </Body>
        <Right>
          <Button transparent>
            <Icon name="globe" style={{ color: '#060606' }} />
          </Button>
        </Right>
      </Header>
      <Content padder>
        {current > 0 || current < pages.length - 1 ? (
          <PreviousNext previous={previous} next={next} current={current} pages={pages} />
        ) : null}
        <View>
          <Markdown style={markdown} rules={rules(images)}>
            {md}
          </Markdown>
        </View>
        {current > 0 || current < pages.length - 1 ? (
          <PreviousNext previous={previous} next={next} current={current} pages={pages} />
        ) : null}
      </Content>
    </Container>
  );
};

// LessonComponent.navigationOptions = ({ current, pages }) => {
//   console.log('pages', pages);
//   console.log('current', current);
//   return {
//     title: index[pages[current].path].meta.title,
//     headerRight: (
//       <Button transparent>
//         <Icon name="globe" style={{ color: '#060606' }} />
//       </Button>
//     )
//   };
// };

const Lesson = compose(pages, current, withNavigation, pure)(LessonComponent);

export default Lesson;
