import React, { Component } from 'react';
import { Image, View, StyleSheet } from 'react-native';

import { StackNavigator } from 'react-navigation';

import { compose, withState, withHandlers, pure } from 'recompose';

import styles from './styles';
import markdown from '../styles/markdown';

import Lessons from '../lessons';
import Lesson from '../lesson';

import { Button, Text, Header, Title, Body, Left, Right, Content, List, ListItem, Icon } from 'native-base';

import Markdown, { getUniqueID } from 'react-native-markdown-renderer';

import index from '../../content';

import { expo } from '../../app.json';

const teaser = compose(
  withState('expanded', 'expand', false),
  withHandlers({
    show: ({ expand }) => e => expand(true),
    hide: ({ expand }) => e => expand(false),
    toggle: ({ expand }) => e => expand(current => !current)
  })
);

const rules = images => ({
  image: (node, children, parent, styles) => {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }} key={getUniqueID()}>
        <Image
          resizeMode="contain"
          style={StyleSheet.flatten([
            // {
            //   flex: 1
            // },
            styles.image
          ])}
          source={images[node.attributes.src]}
          // TODO: This this cannot be done in react-native it might be necessary to generate a
          // resource js file which harvest all images in `content`, require them and address them with a key.
          // source={require('../../content/lessons/know-your-mobile/' + node.attributes.src)}
        />
      </View>
    );
  }
});

const { md, images, meta: { title } } = index['index.md.js'];
const more = md.indexOf();
const SEPARATOR = '<!-- more -->';
const INDEX = 200;
const line = md.indexOf('\n', INDEX);
const split = more !== -1 ? [more, more + SEPARATOR.length] : [line, line + 2];

const pad = (n, width, z) => {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

// Do it stringly typed for first draft
const sections = Object.keys(index)
  .filter(k => k.endsWith('/index.md.js') && index[k].meta.title)
  .sort((before, after) => pad(before.split('-')[0], 5) - pad(after.split('-')[0], 5))
  .map(key => ({
    title: index[key].meta.title,
    key: key.replace('/index.md.js', ''),
    page_count: Object.keys(index).filter(
      k => k.startsWith(key.replace('/index.md.js', '')) && !k.endsWith('/index.md.js')
    ).length
  }));

const HomeComponent = ({ navigation, toggle, expanded }) => {
  return (
    <Content>
      <List>
        <ListItem onPress={() => toggle()}>
          <Body>
            <Markdown style={markdown} rules={rules(images)}>
              {md.slice(0, split[0])}
            </Markdown>
            {!expanded && <Text>more...</Text>}
            {expanded && (
              <Markdown style={markdown} rules={rules(images)}>
                {md.slice(split[1])}
              </Markdown>
            )}
            {expanded && <Text>less...</Text>}
          </Body>
        </ListItem>
        {sections.map(({ title, key, page_count }) => (
          <ListItem key={key} onPress={() => navigation.navigate('Lessons', { key })}>
            <Body>
              <Text>{title}</Text>
              <Text note>{page_count} sections</Text>
            </Body>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
        ))}
      </List>
    </Content>
  );
};

const Home = compose(teaser, pure)(HomeComponent);

Home.navigationOptions = ({ navigation }) => ({
  title: expo.name,
  headerBackTitle: 'Top',
  headerRight: (
    <Button transparent>
      <Icon name="globe" style={{ color: '#060606' }} />
    </Button>
  ),
  headerLeft: (
    <Button transparent onPress={() => navigation.navigate('DrawerOpen')}>
      <Icon name="menu" style={{ color: '#060606' }} />
    </Button>
  )
});

export const HomeNavigator = StackNavigator(
  {
    Home: { screen: Home },
    Lessons: { screen: Lessons },
    Lesson: {
      screen: Lesson,
      navigationOptions: {
        header: null
      }
      //   navigationOptions: props => {
      //     console.log('props', props);
      //     return {
      //       title: index[pages[current].path].meta.title,
      //       headerRight: (
      //         <Button transparent>
      //           <Icon name="globe" style={{ color: '#060606' }} />
      //         </Button>
      //       )
      //     };
      //   }
    }
  },
  {
    headerMode: 'screen',
    cardStyle: {
      backgroundColor: 'white'
    }
  }
);

export default Home;
