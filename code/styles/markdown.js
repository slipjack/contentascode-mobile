const { StyleSheet } = require('react-native');

const markdown = StyleSheet.create({
  heading1: {
    fontSize: 14,
    fontFamily: 'Roboto_lightitalic'
  },
  heading2: {
    fontSize: 26,
    fontFamily: 'Roboto_black',
    paddingVertical: 10
  },
  heading3: {
    fontSize: 18,
    fontFamily: 'Roboto_bold',
    color: '#6F6F6F',
    paddingVertical: 5
  },
  heading4: {
    fontSize: 16,
    fontFamily: 'Roboto_bold',
    color: '#6F6F6F',
    paddingVertical: 5
  },
  heading5: {
    fontSize: 13,
    fontFamily: 'Roboto_bold',
    color: '#6F6F6F',
    paddingVertical: 5
  },
  heading6: {
    fontSize: 11,
    fontFamily: 'Roboto_bold',
    color: '#6F6F6F',
    paddingVertical: 5
  },
  blockquote: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    margin: 20,
    backgroundColor: '#f8f8f8'
  },
  hr: {
    backgroundColor: '#707070',
    height: 2,
    margin: 5
  },
  text: {
    fontSize: 18,
    fontFamily: 'Roboto_light',
    lineHeight: 22
  },
  strong: {
    fontSize: 18,
    fontFamily: 'Roboto_bold',
    lineHeight: 22
  },
  em: {
    fontSize: 18,
    fontFamily: 'Roboto_lightitalic',
    lineHeight: 22
  }
});

export default markdown;
