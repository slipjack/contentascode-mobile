import Expo from 'expo';
import React from 'react';
import App from './code/App';

import getTheme from './native-base-theme/components';

import { Container, StyleProvider } from 'native-base';

export default class App1 extends React.Component {
  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Roboto_light: require('./code/assets/fonts/Roboto-Light.ttf'),
      Roboto_lightitalic: require('./code/assets/fonts/Roboto-LightItalic.ttf'),
      Roboto_black: require('./code/assets/fonts/Roboto-Black.ttf'),
      Roboto_bold: require('./code/assets/fonts/Roboto-Bold.ttf'),
      Roboto_thinitalic: require('./code/assets/fonts/Roboto-ThinItalic.ttf'),
      // Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
      MaterialCommunityIcons: require('@expo/vector-icons/fonts/MaterialCommunityIcons.ttf')
    });

    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (
      <StyleProvider style={getTheme()}>
        <Container>
          <App />
        </Container>
      </StyleProvider>
    );
  }
}
